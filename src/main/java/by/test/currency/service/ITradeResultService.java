package by.test.currency.service;

import java.sql.Date;
import java.util.List;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.TradeResult;

public interface ITradeResultService {
	
	public TradeResult getById(long id);
	public boolean addTradeResult(TradeResult tr);
	public boolean updateTradeResult(TradeResult tr);
	public boolean deleteTradeResult(TradeResult tr);
	public List<TradeResult>getByDate(Date date);
	public TradeResult getByCurrencyAndDate(Currency currency, Date date);


}
