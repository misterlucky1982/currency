package by.test.currency.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.test.currency.dao.ICurrencyDAO;
import by.test.currency.entityes.Currency;

@Service
public class CurrencyService implements ICurrencyService{

	@Autowired
	private ICurrencyDAO dao;
	
	
	@Override
	public Currency getCurrencyByKeyCode(String keycode) {
		try{
			return this.dao.getCurrencyByKeyCode(keycode);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public boolean addCurrency(Currency currency) {
		try{
			return this.dao.addCurrency(currency);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean updateCurrency(Currency currency) {
		try{
			return this.dao.updateCurrency(currency);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean deleteCurrency(Currency currency) {
		try{
			return this.dao.deleteCurrency(currency);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public List<Currency> getCurrencyList() {
		try{
			return this.dao.getCurrencyList();
		}catch(SQLException e){
			return new ArrayList<>();
		}
	}

}
