package by.test.currency.service;


import java.sql.Date;
import java.util.List;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;

public interface IRateService {
	
	public Rate getRate(TradeResult result);
	public boolean addRate(Rate rate);
	public boolean updateRate(Rate rate);
	public boolean deleteRate(Rate rate);
	public List<Rate> getRateListByDate(Date date);

}
