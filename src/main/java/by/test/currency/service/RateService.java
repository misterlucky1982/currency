package by.test.currency.service;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.test.currency.dao.IRateDAO;
import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;

@Service
public class RateService implements IRateService{
	
	@Autowired
	private IRateDAO dao;

	@Override
	public Rate getRate(TradeResult result) {
		try{
			return this.dao.getRate(result);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public boolean addRate(Rate rate) {
		try{
			return this.dao.addRate(rate);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean updateRate(Rate rate) {
		try {
			return this.dao.updateRate(rate);
		} catch (SQLException e) {
			return false;
		}
	}

	@Override
	public boolean deleteRate(Rate rate) {
		try {
			return this.dao.deleteRate(rate);
		} catch (SQLException e) {
			return false;
		}
	}

	@Override
	public List<Rate> getRateListByDate(Date date) {
		try{
			return this.dao.getRateListByDate(date);
		}catch(SQLException e){
			return new ArrayList<>();
		}
	}

}
