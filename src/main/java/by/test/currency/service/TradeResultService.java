package by.test.currency.service;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.test.currency.dao.ITradeResultDAO;
import by.test.currency.entityes.Currency;
import by.test.currency.entityes.TradeResult;

@Service
public class TradeResultService implements ITradeResultService{
	
	@Autowired
	private ITradeResultDAO dao;

	@Override
	public TradeResult getById(long id) {
		try{
			return this.dao.getById(id);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public boolean addTradeResult(TradeResult tr) {
		try{
			return this.dao.addTradeResult(tr);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean updateTradeResult(TradeResult tr) {
		try{
			return this.dao.updateTradeResult(tr);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean deleteTradeResult(TradeResult tr) {
		try{
			return this.dao.deleteTradeResult(tr);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public List<TradeResult> getByDate(Date date) {
		try{
			return this.dao.getByDate(date);
		}catch(SQLException e){
			return new ArrayList<>();
		}
	}

	@Override
	public TradeResult getByCurrencyAndDate(Currency currency, Date date) {
		try {
			return this.dao.getByCurrencyAndDate(currency, date);
		} catch (SQLException e) {
			return null;
		}
	}

	

}
