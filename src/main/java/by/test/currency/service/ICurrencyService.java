package by.test.currency.service;

import java.util.List;

import by.test.currency.entityes.Currency;

public interface ICurrencyService {
	
	public Currency getCurrencyByKeyCode(String keycode);
	public boolean addCurrency(Currency currency);
	public boolean updateCurrency(Currency currency);
	public boolean deleteCurrency(Currency currency);
	public List<Currency> getCurrencyList();

}
