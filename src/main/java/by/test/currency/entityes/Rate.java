package by.test.currency.entityes;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="rates")
public class Rate implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@JoinColumn(name="trade_result")
	@OneToOne(cascade= {CascadeType.REFRESH}, fetch=FetchType.LAZY)
	private TradeResult tradeResult;
	@Column (name = "value")
	private double value;
	
	public Rate(){}
	
	public Rate(TradeResult tr, double rate){
		this.value = rate;
		this.tradeResult = tr;
	}

	public TradeResult getTradeResult() {
		return tradeResult;
	}

	public void setTradeResult(TradeResult tradeResult) {
		this.tradeResult = tradeResult;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tradeResult == null) ? 0 : tradeResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rate other = (Rate) obj;
		if (tradeResult == null) {
			if (other.tradeResult != null)
				return false;
		} else if (!tradeResult.equals(other.tradeResult))
			return false;
		return true;
	}
	
	
}
