package by.test.currency.entityes;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="currencyes")
public class Currency implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column (name = "keycode", nullable = false)
	private String keycode;
	@Column (name = "name", nullable = false)
	private String name;
	
	
	public Currency(){
		
	}
	
	public Currency(String keycode, String name){
		this.keycode = keycode;
		this.name = name;
	}


	public String getKeycode() {
		return keycode;
	}


	public void setKeycode(String keycode) {
		this.keycode = keycode;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keycode == null) ? 0 : keycode.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (keycode == null) {
			if (other.keycode != null)
				return false;
		} else if (!keycode.equals(other.keycode))
			return false;
		return true;
	}
	
	
	

}
