package by.test.currency.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;
import by.test.currency.service.ICurrencyService;
import by.test.currency.service.IRateService;
import by.test.currency.service.ITradeResultService;
import by.test.currency.util.JSONUtils;


@RequestMapping("/currency")
@RestController
public class MainController {
	
	@Autowired 
	private ApplicationContext context;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String currencyList(){
		List<Currency>list=context.getBean(ICurrencyService.class).getCurrencyList();
		return JSONUtils.currencyKeyCodesToJSON(list);
	}
	
	@RequestMapping(value = "/rate", method = RequestMethod.GET)
	public String getCurrencyRate(@RequestParam(name = "keycode", required=true)String keycode){
		Currency cr = this.context.getBean(ICurrencyService.class).getCurrencyByKeyCode(keycode);
		Date today = new Date(System.currentTimeMillis());
		TradeResult tr = this.context.getBean(ITradeResultService.class).getByCurrencyAndDate(cr, today);
		Rate rate = tr.getRate();
		return JSONUtils.currencyRateInformationToJSON(rate);
	}
	
}
