package by.test.currency.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;

public class JSONUtils {
	
	public static String currencyKeyCodesToJSON(List<Currency>list){
		JSONArray json = new JSONArray();
		for(Currency c:list)json.add(c.getKeycode());
		return json.toJSONString();
	}
	
	public static String doubleArrayToJSONString(double...args){
		JSONArray json = new JSONArray();
		for(double val:args)json.add(val);
		return json.toJSONString();
	}
	
	public static String currencyRateInformationToJSON(Rate rate){
		JSONObject ob = new JSONObject();
		if(rate==null)return ob.toJSONString();
		ob.put("currencyName", rate.getTradeResult().getCurrency().getName());
		ob.put("date", rate.getTradeResult().getDate().toString());
		ob.put("rate", rate.getValue());
		return ob.toJSONString();
	}
	
	public static Double parseCurrencRateFromJSON(String json){
		Double result = null;
		JSONParser parser = new JSONParser();
		try {
			JSONObject ob = (JSONObject) parser.parse(json);
			Double rate = (Double)ob.get("Cur_OfficialRate");
			Long scale = (Long)ob.get("Cur_Scale");
			result = rate/scale;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}
