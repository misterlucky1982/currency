package by.test.currency.util;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

public class DataBaseConnectionUtil {

	private static final String DRIVER_CLASS = "org.h2.Driver";
	private static final String URL = "jdbc:h2:file:~/h2/currencyNew;DB_CLOSE_ON_EXIT=TRUE";
	private static final String USERNAME = "sa";
	private static final String PASSWORD = "";
	private static final SessionFactory SESSION_FACTORY;
	private static final JdbcTemplate JDBC_TEMPLATE;
	private static final DataSource DATA_SOURCE;
	static {
		try {
			SESSION_FACTORY = getConfiguration().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	static {
		DATA_SOURCE = new org.springframework.jdbc.datasource.DriverManagerDataSource(URL, USERNAME, PASSWORD);
	}
	static {
		JDBC_TEMPLATE = new JdbcTemplate(DATA_SOURCE);
	}

	public static SessionFactory getSessionFactory() {
		return SESSION_FACTORY;
	}

	@Bean
	public static Configuration getConfiguration() {
		Configuration configuration = new Configuration().setProperty("hibernate.connection.driver_class", DRIVER_CLASS)
				.setProperty("hibernate.connection.url", URL).setProperty("hibernate.connection.username", USERNAME)
				.setProperty("hibernate.connection.password", PASSWORD)
				.setProperty("hibernate.connection.autocommit", "false")
				.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider")
				.setProperty("hibernate.cache.use_second_level_cache", "false")
				.setProperty("hibernate.cache.use_query_cache", "false")
				.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
				.setProperty("hibernate.show_sql", "true")
				.setProperty("hibernate.current_session_context_class",
						"org.hibernate.context.internal.ThreadLocalSessionContext")
				.setProperty("hibernate.enable_lazy_load_no_trans", "true")
				.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "true")
				.addAnnotatedClass(by.test.currency.entityes.Currency.class)
				.addAnnotatedClass(by.test.currency.entityes.TradeResult.class)
				.addAnnotatedClass(by.test.currency.entityes.Rate.class);

		return configuration;
	}

	@Bean
	public static JdbcTemplate getJdbcTemplate() {
		return JDBC_TEMPLATE;
	}

	@Bean
	public static DataSource getDataSource() {
		return DATA_SOURCE;
	}

	public static void dataBaseInitialization() {
		String createCurrencyes = "create table currencyes (keycode character varying (3) not null unique, name character varying(40), primary key (keycode));";
		String createTradeResults = "create table trade_results (id serial, currency character varying (3) not null references currencyes (keycode), date date not null, primary key(id));";
		String createRates = "create table rates (trade_result bigint not null unique references trade_results(id), value decimal (10,3), primary key (trade_result));";
		String createSequence = "CREATE SEQUENCE HIBERNATE_SEQUENCE START WITH 1 INCREMENT BY 1;";
		try {
			JDBC_TEMPLATE.execute(createCurrencyes);
			System.out.println("table 'currencyes' is created");
		} catch (Exception e) {
			System.out.println("table 'currencyes' is allready exists");
		}
		try {
			JDBC_TEMPLATE.execute(createTradeResults);
			System.out.println("table 'trade_results' is created");
		} catch (Exception e) {
			System.out.println("table 'trade_results' is allready exists");
		}
		try {
			JDBC_TEMPLATE.execute(createRates);
			System.out.println("table 'rates' is created");
		} catch (Exception e) {
			System.out.println("table 'rates' is already exists");
		}
		try {
			JDBC_TEMPLATE.execute(createSequence);
			System.out.println("hibernate_sequence is created");
		} catch (Exception e) {
			System.out.println("hibernate_sequence is allready exists");
		}
	}

}
