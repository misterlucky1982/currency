package by.test.currency.util;

public class CurencyUtil implements ICurrencyPropertyKeeper{

	@Override
	public String[] keyCodes() {
		// TODO Auto-generated method stub
		String cur[] =  {"EUR","USD","RUB"};
		return cur;
	}

	@Override
	public String getUrlLineForRateRequest(String key) {
		switch(key){
		case "USD":
			return "http://www.nbrb.by/API/ExRates/Rates/145";
		case "RUB":
			return "http://www.nbrb.by/API/ExRates/Rates/298";
		case "EUR":
			return "http://www.nbrb.by/API/ExRates/Rates/EUR?ParamMode=2";
		default:
			return null;
		}
	}

}
