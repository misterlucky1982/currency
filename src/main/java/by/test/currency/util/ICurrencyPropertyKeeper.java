package by.test.currency.util;

public interface ICurrencyPropertyKeeper {

	String[] keyCodes();
	String getUrlLineForRateRequest(String key);
}
