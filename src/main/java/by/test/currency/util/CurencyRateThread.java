package by.test.currency.util;

import java.io.IOException;
import java.sql.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.context.ApplicationContext;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;
import by.test.currency.service.ICurrencyService;
import by.test.currency.service.IRateService;
import by.test.currency.service.ITradeResultService;

public class CurencyRateThread implements Runnable{
	
	
	private ApplicationContext context;
	private ICurrencyPropertyKeeper currencyes = new CurencyUtil();
	
	public CurencyRateThread(ApplicationContext context){
		this.context = context;
	}
	
	
	@Override
	public void run() {
		while(true){
			System.out.println("Sending new request...");
			Date now = new Date(System.currentTimeMillis());
			for(String key:this.currencyes.keyCodes()){
				System.out.println(key+" : ");
				Currency cr = this.context.getBean(ICurrencyService.class).getCurrencyByKeyCode(key);
				TradeResult tr = new TradeResult(now,cr);
				String json = null;
				try {
					json= HTTPUtil.doGET(this.currencyes.getUrlLineForRateRequest(key));
				} catch (IOException e) {
					e.printStackTrace();
					continue;
				}
				Double rate = JSONUtils.parseCurrencRateFromJSON(json);
				if(rate==null)continue;
				System.out.println(rate);
				Rate currate = new Rate(tr, rate);
				this.context.getBean(ITradeResultService.class).addTradeResult(tr);
				this.context.getBean(IRateService.class).addRate(currate);
				tr.setRate(currate);
				this.context.getBean(ITradeResultService.class).updateTradeResult(tr);
			}
			try {
				Thread.currentThread().sleep(1800000);//30min
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
