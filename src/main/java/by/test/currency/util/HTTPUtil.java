package by.test.currency.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class HTTPUtil {

	public static  String doGET(String url) throws IOException {
		Charset utf8 = Charset.forName("UTF-8");
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setConnectTimeout(30000);
		BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
		int next;
		ArrayList<Byte> bytes = new ArrayList<>();
		while ((next = bis.read()) != -1) {
			bytes.add((byte) next);
		}
		bis.close();
		byte[] bytes2 = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++)
			bytes2[i] = bytes.get(i);
		return new String(bytes2, utf8);
	}
	
	
}
