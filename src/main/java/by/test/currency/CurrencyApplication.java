package by.test.currency;

import java.io.IOException;
import java.sql.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;
import by.test.currency.service.ICurrencyService;
import by.test.currency.service.IRateService;
import by.test.currency.service.ITradeResultService;
import by.test.currency.util.CurencyRateThread;
import by.test.currency.util.HTTPUtil;
import by.test.currency.util.JSONUtils;


@SpringBootApplication
public class CurrencyApplication {
	

	public static void main(String[] args) {
		
		ApplicationContext context = SpringApplication.run(CurrencyApplication.class, args);
		by.test.currency.util.DataBaseConnectionUtil.dataBaseInitialization();
		standAloneApplicationThreadStart(context);
	}
	
	public static void standAloneApplicationThreadStart(ApplicationContext context){
		Currency usd = new Currency("USD", "Доллар США");
		Currency eur = new Currency("EUR", "Евро");
		Currency rur = new Currency("RUB", "Российский рубль");
		ICurrencyService currencyService = context.getBean(ICurrencyService.class);
		System.out.println("EUR added: "+(currencyService.addCurrency(eur)));
		System.out.println("USD added: "+(currencyService.addCurrency(usd)));
		System.out.println("RUR added: "+(currencyService.addCurrency(rur)));
		new Thread(new CurencyRateThread(context)).start();
	}
	

	@Deprecated
	public static void fillingDataBaseWithTestData(ApplicationContext context){
		System.out.println("starting fill database with test data...");
		Currency usd = new Currency("USD", "Доллар США");
		Currency eur = new Currency("EUR", "Евро");
		Currency rur = new Currency("RUB", "Российский рубль");
		Currency xyz = new Currency("XYZ", "Заморский тугрик");
		ICurrencyService currencyService = context.getBean(ICurrencyService.class);
		System.out.println("EUR added: "+(currencyService.addCurrency(eur)));
		System.out.println("USD added: "+(currencyService.addCurrency(usd)));
		System.out.println("RUR added: "+(currencyService.addCurrency(rur)));
		System.out.println("XYZ added: "+(currencyService.addCurrency(xyz)));
		Date today = new Date(System.currentTimeMillis());
		ITradeResultService tradeService = context.getBean(ITradeResultService.class);
		TradeResult tradeUSD = new TradeResult(today,usd);
		TradeResult tradeEUR = new TradeResult(today,eur);
		TradeResult tradeRUR = new TradeResult(today,rur);
		TradeResult tradeXYZ = new TradeResult(today, xyz);
		Rate rateUSD = new Rate(tradeUSD, 2.15);
		Rate rateEUR = new Rate(tradeEUR, 2.33);
		Rate rateRUR = new Rate(tradeRUR, 0.044);
		Rate rateXYZ = new Rate(tradeXYZ, 99999.456);
		IRateService rateService = context.getBean(IRateService.class);
		System.out.println("usd is traded: "+(tradeService.addTradeResult(tradeUSD)));
		System.out.println("eur is traded: "+(tradeService.addTradeResult(tradeEUR)));
		System.out.println("rur is traded: "+(tradeService.addTradeResult(tradeRUR)));
		System.out.println("xyz is traded: "+(tradeService.addTradeResult(tradeXYZ)));
		System.out.println("usd is rated: "+(rateService.addRate(rateUSD)));
		System.out.println("eur is rated: "+(rateService.addRate(rateEUR)));
		System.out.println("rur is rated: "+(rateService.addRate(rateRUR)));
		System.out.println("xyz is rated: "+(rateService.addRate(rateXYZ)));
		tradeUSD.setRate(rateUSD);
		tradeEUR.setRate(rateEUR);
		tradeRUR.setRate(rateEUR);
		tradeXYZ.setRate(rateXYZ);
		System.out.println("usd is updated: "+(tradeService.updateTradeResult(tradeUSD)));
		System.out.println("eur is updated: "+(tradeService.updateTradeResult(tradeEUR)));
		System.out.println("rur is updated: "+(tradeService.updateTradeResult(tradeRUR)));
		System.out.println("xyz is updated: "+(tradeService.updateTradeResult(tradeXYZ)));
		System.out.println("filling database with test data has finished...");
	}
}

