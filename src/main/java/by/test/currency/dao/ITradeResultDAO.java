package by.test.currency.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.TradeResult;

public interface ITradeResultDAO {
	
	public TradeResult getById(long id) throws SQLException;
	public boolean addTradeResult(TradeResult tr) throws SQLException;
	public boolean updateTradeResult(TradeResult tr) throws SQLException;
	public boolean deleteTradeResult(TradeResult tr) throws SQLException;
	public List<TradeResult>getByDate(Date date) throws SQLException;
	public TradeResult getByCurrencyAndDate(Currency currency, Date date) throws SQLException;

}
