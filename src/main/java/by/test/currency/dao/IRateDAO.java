package by.test.currency.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;

public interface IRateDAO {

	public Rate getRate(TradeResult result) throws SQLException;
	public boolean addRate(Rate rate) throws SQLException;
	public boolean updateRate(Rate rate) throws SQLException;
	public boolean deleteRate(Rate rate) throws SQLException;
	public List<Rate> getRateListByDate(Date date) throws SQLException;
		
}
