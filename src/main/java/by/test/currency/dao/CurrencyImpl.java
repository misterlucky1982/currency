package by.test.currency.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import by.test.currency.entityes.Currency;

@Transactional
@Repository
public class CurrencyImpl implements ICurrencyDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Currency getCurrencyByKeyCode(String keycode) throws SQLException {
		Session session = null;
		Currency item=null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			item = (Currency) session.get(Currency.class, keycode);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return item;
	}

	@Override
	public boolean addCurrency(Currency currency) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(currency);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean updateCurrency(Currency currency) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(currency);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteCurrency(Currency currency) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(currency);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public List<Currency> getCurrencyList() throws SQLException {
		Session session = null;
		List<Currency> items = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			items = session.createQuery("from Currency").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		if(items!=null){
			return items;
		}else return new ArrayList<Currency>();
	}
	

}
