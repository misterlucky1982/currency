package by.test.currency.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import by.test.currency.entityes.Currency;
import by.test.currency.entityes.TradeResult;

@Transactional
@Repository
public class TradeResultImpl implements ITradeResultDAO{

	@Override
	public TradeResult getById(long id) throws SQLException {
		Session session = null;
		TradeResult item=null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			item = (TradeResult) session.get(TradeResult.class, id);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return item;
	}

	@Override
	public boolean addTradeResult(TradeResult tr) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(tr);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean updateTradeResult(TradeResult tr) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(tr);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteTradeResult(TradeResult tr) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(tr);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public List<TradeResult> getByDate(Date date) throws SQLException {
		Session session = null;
	    List<TradeResult> list = null;
	    try {
	      session = by.test.currency.util.DataBaseConnectionUtil
	    		  .getSessionFactory()
	    		  .getCurrentSession();
	      session.beginTransaction();
	      Query query = null;
	      String hql = "from TradeResult where date = :date";	      
	      query = session.createQuery(hql).setParameter("date", date);
	      list = (List<TradeResult>) query.getResultList();
	      session.getTransaction().commit();
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    if(list==null){
	    	return new ArrayList<TradeResult>();
	    }else return list;
	}

	@Override
	public TradeResult getByCurrencyAndDate(Currency currency, Date date) throws SQLException {
		Session session = null;
		TradeResult result = null;
	    try {
	      session = by.test.currency.util.DataBaseConnectionUtil
	    		  .getSessionFactory()
	    		  .getCurrentSession();
	      session.beginTransaction();
	      Query query = null;
	      String hql = "from TradeResult where date = :date and currency = :curr";	      
	      query = session.createQuery(hql)
	    		  .setParameter("date", date)
	    		  .setParameter("curr", currency);
	      List<TradeResult>list = (List<TradeResult>) query.getResultList();
	      if(list.size()>0)result=list.get(list.size()-1);
	      session.getTransaction().commit();
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    return result;
	}

}
