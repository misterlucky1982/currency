package by.test.currency.dao;

import java.sql.SQLException;
import java.util.List;

import by.test.currency.entityes.Currency;

public interface ICurrencyDAO {
	
	public Currency getCurrencyByKeyCode(String keycode) throws SQLException;
	public boolean addCurrency(Currency currency) throws SQLException;
	public boolean updateCurrency(Currency currency) throws SQLException;
	public boolean deleteCurrency(Currency currency) throws SQLException;
	public List<Currency> getCurrencyList() throws SQLException;

}
