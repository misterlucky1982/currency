package by.test.currency.dao;


import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import by.test.currency.entityes.Rate;
import by.test.currency.entityes.TradeResult;

@Transactional
@Repository
public class RateImpl implements IRateDAO{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Rate getRate(TradeResult result) throws SQLException {
		Session session = null;
		Rate item=null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			item = (Rate) session.get(Rate.class, result);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return item;
	}

	@Override
	public boolean addRate(Rate rate) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(rate);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean updateRate(Rate rate) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(rate);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteRate(Rate rate) throws SQLException {
		Session session = null;
		try {
			session = by.test.currency.util.DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(rate);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public List<Rate> getRateListByDate(Date date) throws SQLException {
		Session session = null;
		List<Rate>result = null;
	    try {
	      session = by.test.currency.util.DataBaseConnectionUtil
	    		  .getSessionFactory()
	    		  .getCurrentSession();
	      session.beginTransaction();
	      Query query = null;
	      String hql = "from Rate where tradeResult.date = :date1";	      
	      query = session.createQuery(hql)
	    		  .setParameter("date1", date);
	      result = (List<Rate>) query.getResultList();
	      session.getTransaction().commit();
	    } finally {
	      if (session != null && session.isOpen()) {
	        session.close();
	      }
	    }
	    if(result==null)result=new ArrayList<>();
	    return result;
	}

	

}
