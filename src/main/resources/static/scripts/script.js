/**
 * 
 */

var url = "http://env-2982050.mycloud.by/";
document.addEventListener("DOMContentLoaded", initialization);
var currencyes = [];

function initialization() {
	$('#choose_currency').html("ПОЛУЧИТЬ КУРС");
	currencyListRequest();
	$('#choose_currency').click(sendingCurrencyRateRequest);
}

function currencyListRequest() {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url + '/currency/list', true);
	xhr.send();
	xhr.onreadystatechange = function() {
		if (this.readyState != 4)
			return;
		if (this.status != 200) {
		} else {
			var data = JSON.parse(this.responseText);
			for (item in data) {
				currencyes.push(data[item]);
			}
			comboboxInitialization();
		}
	};
}

function comboboxInitialization() {
	var combobox = document.getElementById('currency_type')
	console.log(currencyes)
	for ( var i in currencyes) {
		item = document.createElement("option")
		item.text = currencyes[i];
		item.value = currencyes[i];
		try {
			combobox.add(item, null); // Standard
		} catch (error) {
			combobox.add(item); // IE only
		}
	}
}

function sendingCurrencyRateRequest() {
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', url + '/currency/rate?keycode='+$('#currency_type').val(), true);
	xhr.send();
	xhr.onreadystatechange = function() {
		if (this.readyState != 4)
			return;
		if (this.status != 200) {
		} else {
			currencyInfo(JSON.parse(this.responseText));
		}
	};
}

function currencyInfo(data){
	var info = "";
	if(data.length==0){
		info = "не удалось получить информацию о курсе";
	}else{
		info = data.currencyName+" (курс на "+data.date+") : "+data.rate+" BYN";
	}
	$('#info').html(info);	
}


